#!/usr/bin/python3

import random

von=2
bis=4

quelle = "0163-77736"

ziele = [
"0173-2515758",
"0174-1287006",
"0151-953550729",
"0173-426881",
"0161-64322",
"0161-310967",
"0173-26802",
"0162-85846",
"0162-415300",
]

def doppel(v :int, b :int)->String:
    """
    Erstellen zweistelliger Ziffernpaare
    """
    n = random.randint(v,b)
    if n < 10:
        nummer = "0" + str(n)
    else:
        nummer = str(n)
    return nummer

def dauer():
    """
    Dauer eines Telefonats erfinden, Max 00:59:59
    """
    res = "00:" + doppel(0,59) + ":" + doppel(0,59)
    return res

def zeit():
    """
    Eine Uhrzeit erstellen (für den Anruf)
    """
    res = doppel(0,23) + ":" + doppel(0,59) + ":" + doppel(0,59)
    return res

def create_addons():
    """
    Komplette Telefonanrufe erfinden. von, bis oben festgelegt
    """
    for ziel in ziele:
        rep = random.randint(von, bis)
        for n in range(rep):
            print(quelle + "\t" + ziel + "\t" + zeit() + "\t" + dauer()) 

def create_reverse_addons():
    """
    Anrufe in die andere Richtung generieren
    """
    for ziel in ziele:
        rep = random.randint(von, bis)
        for n in range(rep):
            print(ziel + "\t" + quelle + "\t" + zeit() + "\t" + dauer())

def del_addons():
    """
    Nummern aus der Listendatei löschen -- warum auch immer
    """
    for nummer in ziele:
        print("grep -v " + nummer + " liste1.csv > tmp.csv && mv tmp.csv liste1.csv") 


create_reverse_addons()
